import matplotlib.pyplot as plt


from src.song_avg import gen_stats


def main():
    """
    See README.md for documentation on how to run this program.
    """
    results = gen_stats()
    print(results)

    plt.bar(x=results.columns, height=results.loc['Mean Average', :])
    plt.ylabel('Mean Average Word Count of Songs')
    plt.show()


if __name__ == '__main__':
    main()
