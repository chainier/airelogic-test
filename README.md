# airelogic-test

An application addressing the task supplied by AireLogic.

## Authors
> Benjamin Tayler-Barrett (benjamin.taylerbarrett@gmail.com)

## Installation
The use of a virtual environment is highly recommended!

To download the program:
`git clone https://gitlab.com/chainier/airelogic-test.git`

`cd airelogic-test`

`pip install -r requirements.txt`

You should now be ready to run the program.

## Usage
While in the `airelogic-test` directory, and you have decided which artists you would 
like a statistical breakdown for the following command will run the program:

`python run.py -a 'artist name'`

You can also chain multiple artists if you would like information on more than one:

`python run.py -a 'artist name' -a 'artist name'`

There is no upper limit to the number of artists than can be requested. 
It should also be noted, that if the artist you are interested in contains more than word 
(e.g. most individuals have a firstname and surname), you **MUST** pass the artist name inside quotation marks.

For example,`'Lewis Capaldi'` will find you information on Lewis Capaldi. Where as `Lewis Capaldi` will find 
you nothing at all.

For each artist that you have passed to the program, the experience will be as follows:
* You will be prompted to select the correct artist from a list of the ten most likely based on
what you passed to the program. More often than not, it will be option 1.

* After a short time, a status bar will appear as the program collects the lyrics for the songs.

* You will then be prompted for the next artist, etc.

Once all of the artists have had all songs collected, you will be presented with a short list of
basic descriptive statistics for each of the artists.

## Known Issues
* If an artist is selected, but they have not actually released any songs, the program will crash.

* The time taken to collect songs is rather long. By all means, search for `'Bruce Springsteen'`,
just make sure you don't have a meeting to get to in the near future...

* Distinct lack of Unit Tests...

## Future Features
* Get album list by artist, then song list by album. Though this may miss B-sides for older artists.
But it would significantly improve run time.

* All the unit tests...

* Create a database for previously searched artists, to improve access times for re-runs.
