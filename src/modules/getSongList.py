import time
import json


from . import WebDataCollector


def get_song_list(id_tag, song_list=[], offset=0):
    """
    get_song_list generates a list of songs authored by the artist
    associated with the passed musicbrainz ID.

    Parameters
    ----------
    :param id_tag: (String) Musicbrainz ID for a musical artist.
    :param song_list: (List[String]) A list of song titles.
    :param offset: (Int) An offset to use with API pagination.

    Returns
    -------
    :return SongList: (List[String]) A list of song titles.
    """
    api_call = f'https://musicbrainz.org/ws/2/work?artist={id_tag}&limit=100&offset={offset}&fmt=json'
    api_header = {'User-Agent': 'AverageLengthofSongs/0.0.1 (benjamin.taylerbarrett@gmail.com)'}

    api_response_data = WebDataCollector(url=api_call, headers=api_header, warn=True)

    if api_response_data is not None:
        response_data_json = json.loads(api_response_data)

        for release in response_data_json['works']:
            song_list.append(release['title'])

        while offset < response_data_json['work-count']:
            offset += 100
            time.sleep(0.1)
            api_call = f'https://musicbrainz.org/ws/2/work?artist={id_tag}&limit=100&offset={offset}&fmt=json'
            additional_response_data = WebDataCollector(url=api_call, headers=api_header)

            try:
                additional_response_json = json.loads(additional_response_data)
            except TypeError:
                time.sleep(1)

            for release in additional_response_json['works']:
                song_list.append(release['title'])

        return song_list
