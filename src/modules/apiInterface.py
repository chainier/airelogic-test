import certifi
import urllib3


def WebDataCollector(url='', headers=None, warn=False):
    """
    WebDataCollector takes a URL and dictionary of headers.
    WebDataCollector then makes an API 'Get' request to the passed URL
    checks the SSL certificate, and if all is well, returns a JSON
    object containing the server response.

    If all is not well with the certificate verification, the program
    will terminate with an SSLError.

    Parameters
    ----------
    :param url: (String) The URL from which the request is to be made.
    :param headers: (Dict{String: String}) Headers that are required
                                           by the API.
    :param warn: (Bool) If API return message information is desired.

    Returns
    -------
    :return None: (None) Returns `None` if the API call does not result
                         in a `200` status.
    :return api_request.data: (JSON) The data returned by the API.
    """
    connection_manager = urllib3.PoolManager(cert_reqs='CERT_REQUIRED',
                                             ca_certs=certifi.where())

    api_request = connection_manager.request('GET',
                                             url,
                                             headers=headers)

    if api_request.status != 200:
        response = f"""
The data requested from 
{url} 
was unsuccessful the error code was:
{api_request.status}
"""
        if warn:
            print(response)

        return None

    else:
        if warn:
            print(f'API call to {url} was successful')

        return api_request.data
