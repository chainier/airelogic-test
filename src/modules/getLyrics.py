import json


from . import WebDataCollector


def get_lyrics(artist, song):
    """
    get_lyrics gets the lyrics for a passed song, by a passed artist.

    Parameters
    ----------
    :param artist: (String) The name of the artist of the song to search
                            for.
    :param song: (String) The name of the song to search for.

    Returns
    -------
    :return lyrics: (List[String]) A list of words in a song.
    :return None: (NoneType) None type object.
    """
    artist = artist.replace(' ', '%20')
    song = song.replace(' ', '%20')
    song = song.replace('?', '%3F')
    api_call = f'https://private-anon-ba37b065f6-lyricsovh.apiary-proxy.com/v1/{artist}/{song}'
    api_response_data = WebDataCollector(url=api_call)

    if api_response_data is not None:
        api_response_json = json.loads(api_response_data)

        lyrics = api_response_json['lyrics']
        lyrics = lyrics.replace('\n\n', ' ')
        lyrics = lyrics.replace('\n', ' ')

        return lyrics.split(' ')
    else:
        return None
