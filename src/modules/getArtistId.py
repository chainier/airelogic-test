import sys
import json


from . import WebDataCollector


def get_artist_id(artist_name):
    """
    get_artist_id takes a user supplied musical artists name and
    shows the first ten matches for that artists name in the musicbrainz
    database.

    The user is then prompted to select one of them. Upon selection,
    get_artist_id will return the musicbrainz ID and artist name, as
    recorded in the musicbrainz database.

    Parameters
    ----------
    :param artist_name: (String) The name of a musical artist.

    Returns
    -------
    :return id_tag: (String) The musicbrainz ID for the given artist.
    :return name: (String) The name of the musical artist, as held in
                           the musicbrainz database.
    """
    artist = artist_name.lower().replace(' ', '+')
    api_call = f'https://musicbrainz.org/ws/2/artist?query={artist}&limit=10&offset=0&fmt=json'
    api_header = {'User-Agent': 'AverageLengthofSongs/0.0.1 (benjamin.taylerbarrett@gmail.com)'}

    api_response_data = WebDataCollector(url=api_call, headers=api_header)

    if api_response_data is not None:
        response_data_json = json.loads(api_response_data)

        artists_dict = {}
        for i, artist in enumerate(response_data_json['artists']):
            artists_dict[i + 1] = [artist['name'], artist['id']]

        for k, v in artists_dict.items():
            print(f'{k}. {v[0]}')

        if not artists_dict:
            print('That artist has not been found, have you spelled it correctly?')
            sys.exit(0)

        happy = False
        while not happy:
            user_selection = input(
                'Please enter the number shown before the artist you are interested in: ')
            try:
                id_tag = artists_dict[int(user_selection)][1]
                name = artists_dict[int(user_selection)][0]
                happy = True
            except (KeyError, ValueError) as e:
                print('Unfortunately, that is not one of the available options. Please try again.')

        return id_tag, name
