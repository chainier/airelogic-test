import argparse


def CLIEntryPoint():
    """
    CLIEntryPoint creates a command line argument parser as an entry
    point to the program.

    Parameters
    ----------
    CLIEntryPoint takes no parameters.

    Returns
    -------
    :return args: (argparse Object) An argument parser object.
    """
    parser = argparse.ArgumentParser(
        description='A program to calculate the mean word count of songs by a given artist'
    )
    parser.add_argument(
                    '-a',
                    '--artist',
                    action='append',
                    help='The correctly spelled name of a musical artist',
                    required=True
    )

    args = parser.parse_args()

    return args
