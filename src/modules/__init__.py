from .apiInterface import WebDataCollector
from .entryPoint import CLIEntryPoint
from .getArtistId import get_artist_id
from .getSongList import get_song_list
from .getLyrics import get_lyrics
