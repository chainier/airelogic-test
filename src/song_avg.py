import pandas as pd
from tqdm import tqdm


from . import modules


def gen_stats():
    """
    Parameters
    ----------
    gen_stats takes no parameters.

    Returns
    -------
    :return results: (pandas.DataFrame) A dictionary of dictionaries in which the top level key is the
                                        name of an artist. With each artist, is a dictionary containing
                                        statistic name: statistic value.
    """
    args = modules.CLIEntryPoint()

    results = {}

    for query in args.artist:
        artist_id, artist = modules.get_artist_id(query)
        artist_songs = modules.get_song_list(artist_id)
        songs_frame = pd.DataFrame(data={'SongList': artist_songs})
        songs_frame = songs_frame.drop_duplicates(keep='first')

        songs_frame['LyricsList'] = None
        for i in tqdm(songs_frame.index, desc=f'Getting Lyrics by {artist}'):
            songs_frame.loc[i, 'LyricsList'] = modules.get_lyrics(
                                                        artist.replace('/', ''),
                                                        songs_frame.loc[i, 'SongList'])

        songs_frame = songs_frame.dropna()
        songs_frame['LyricCount'] = songs_frame['LyricsList'].apply(lambda lyrics: len(lyrics))
        songs_frame = songs_frame[songs_frame['LyricCount'] > 1]

        stats = songs_frame['LyricCount'].describe()

        results[artist] = {
            'Mean Average': stats.loc['mean'],
            'Standard Deviation': stats.loc['std'],
            'Minimum Words in a Song': stats.loc['min'],
            'Maximum Words in a Song': stats.loc['max'],
            'Median Words in a Song': stats.loc['50%'],
            'Modal Words in a Song': songs_frame['LyricCount'].mode().loc[0]
        }

    return pd.DataFrame(results)
